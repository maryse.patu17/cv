import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormationComponent } from './component/formation/formation.component';
import { AppComponent } from './app.component';
import { ExperienceComponent } from './component/experience/experience.component';


const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot([
    { path: 'accueil', component: AppComponent },
    { path: 'formation', component: FormationComponent },
    { path: 'experience', component: ExperienceComponent },
    {path: '', redirectTo: '/accueil', pathMatch: 'full'},
  ]),
 ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
