import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormationComponent } from './component/formation/formation.component';
import { ExperienceComponent } from './component/experience/experience.component';
import { SkillComponent } from './component/skill/skill.component';
import { NavComponent } from './component/nav/nav.component';
import { PresentationComponent } from './component/presentation/presentation.component';

@NgModule({
  declarations: [
    AppComponent,
    FormationComponent,
    ExperienceComponent,
    SkillComponent,
    NavComponent,
    PresentationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
